## BTCAM_SPP Bluetooth camera ESP32 demo

This is a simple demo showing how esp camera images can be streamed over bluetooth. Based on the ESP-IDF BT-SPP-VFS-ACCEPTOR example.

The video stream is enabled by sending a non 0 byte to the ESP32, it can be disabled by sending a 0 byte.

Some example code for Android to receive the video is here: https://gitlab.com/-/snippets/2540056 

### Hardware Required

This example is designed to run on the ESP32 ai thinker cam board.

### Build and flash the project

Setup the environment
```
source ~/esp/esp-idf/export.sh
```

Flash the ESP32
```
idf.py flash monitor
```

